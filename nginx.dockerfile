FROM nginx:stable-alpine
#FROM nginx:1.13-alpine

ADD ./nginx/nginx.conf /etc/nginx/nginx.conf
ADD ./nginx/default.conf /etc/nginx/conf.d/default.conf

RUN mkdir -p /var/www/html

RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel

RUN chown laravel:laravel /var/www/html

RUN chmod 777 -R /var/www/html

RUN apk update

RUN apk add -u  --no-cache  --repository http://dl-cdn.alpinelinux.org/alpine/edge/community php php-xml php-dom php-sodium php-xmlwriter php-fileinfo php-tokenizer php-session php-pdo php-mysqli php-pdo_mysql php-mbstring php-phar php-json php-iconv

RUN apk add composer

ADD composer.sh .

RUN chmod +x composer.sh

#RUN ./composer.sh

CMD ./composer.sh
