<?php
namespace App\Repositories;

use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Models\Product;

class ProductRepository implements ProductRepositoryInterface {
    public function list(){
        return Product::all();
    }

    public function getByProductID($pid){
        return Product::find($pid);
    }
}
