<?php
namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface{
    public function list(){
        return User::all();
    }

    public function getByUserID($uid){
        return User::find($uid);
    }
}
