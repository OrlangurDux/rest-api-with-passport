<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Http\Resources\User as UserResource;
use App\Models\User;

class UserController extends BaseController{
    //
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository){
        $this->userRepository = $userRepository;
    }
    /**
     * @OA\Get(
     ** path="/v1/users",
     *   tags={"User"},
     *   summary="Users list",
     *   operationId="users",
     *
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function index(){
        $users = $this->userRepository->list();
        return $this->sendResponse(UserResource::collection($users), 'Users retrieved successfully.');
    }
    /**
     * @OA\Get(
     ** path="/v1/users/{id}",
     *   tags={"User"},
     *   summary="User one",
     *   operationId="user_one",
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function show($id){
        $user = $this->userRepository->getByUserID($id);
        return $this->sendResponse(new UserResource($user), 'User retrieved successfully.');
    }
    /**
     * @OA\DELETE(
     ** path="/v1/users/{id}",
     *   tags={"User"},
     *   summary="User one",
     *   operationId="user_delete",
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     *   @OA\Response(
     *      response=201,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function destroy($id){
        $user = $this->userRepository->getByUserID($id);
	User::findOrFail($id)->delete();
        return $this->sendResponse(new UserResource($user), 'User retrieved destroy.');
    }
}
