# REST API with Passport container

Docker-compose with docker container file nginx, php, composer.

## Usage

To get started, make sure you have [Docker installed](https://docker.com) on your system, and then clone this repository.

For start compose file `docker-compose up -d --build site`.

For stop compose file ``docker-compose down``.

After that completes, follow the steps from the ``src`` file to get your Laravel project added.

- **nginx** - `:3030`
- **mysql** - `:3306`
- **php** - `:9000`

Three additional containers are included that handle Composer, NPM, and Artisan commands *without* having to have these platforms installed on your local computer. Use the following command examples from your project root, modifying them to fit your particular use case.

- `docker-compose run --rm composer update`
- `docker-compose run --rm npm run dev`
- `docker-compose run --rm artisan migrate` 

## Persistent MySQL Storage

Create a `mysql` folder in the project root, alongside the `nginx` and `src` folders.

## API documentation
For generate API documentation use Swagger.io

Link for access ``/api/documentation``