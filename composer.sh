#!/bin/sh
VENDOR_DIR="/var/www/html/vendor"
WORK_DIR="/var/www/html"
B_VENDOR=0
cd "$WORK_DIR"
echo "$VENDOR_DIR"
if [ ! -d "$VENDOR_DIR" ]; then
#  composer update
#else
  if [ ! -f "$WORK_DIR/.env" ]; then
    B_VENDOR=1
    cp .env.example .env
  fi
  COMPOSER_MEMORY_LIMIT=-1 composer install
  sleep 5
  if [ "$B_VENDOR" -eq "1" ]; then
    php artisan key:generate
    php artisan cache:clear
    php artisan config:cache
    php artisan migrate
    php artisan passport:install 
    php artisan storage:link
  fi
else
  php artisan migrate
  php artisan cache:clear
  php artisan config:cache
fi
  chown laravel:laravel -R /var/www/html
  chmod 777 -R /var/www/html
  /usr/sbin/nginx
  tail -f /dev/null
